#!/bin/bash
if [[ -z "$HOSTNAME_FQDN" ]]; then
  export HOSTNAME_FQDN="${NAMESPACE}.web.cern.ch"
fi

# Ensure that assigned uid has entry in /etc/passwd.
date
if [ `id -u` -ge 10000 ]; then
cat /etc/passwd | sed -e "s/^$NB_USER:/builder:/" > /tmp/passwd
echo "foswiki:x:`id -u`:`id -g`:Dummy account for certain commands:/home/foswiki:/bin/bash" >> /tmp/passwd
cat /tmp/passwd > /etc/passwd
rm /tmp/passwd
fi
#
# Prepare Foswiki inside the container
#
cd /tmp/foswiki 
    tools/configure -save -noprompt 
    tools/configure -save -set {DefaultUrlHost}='https://NAMESPACE.web.cern.ch' 
    tools/configure -save -set {ScriptUrlPath}='/foswiki/bin' 
    tools/configure -save -set  {ScriptUrlPaths}{view}='' 
    tools/configure -save -set {PubUrlPath}='/foswiki/pub' 
    tools/configure -save -set {SafeEnvPath}='/bin:/usr/bin' 
    tools/configure -save -set {Store}{Implementation}='Foswiki::Store::RcsWrap' 
    tools/configure -save -set {XSendFileContrib}{Header}='X-Accel-Redirect' 
    tools/configure -save -set {XSendFileContrib}{Location}='/files' 
    tools/configure -save -set {Password}='pete-foswiki' 
    tools/configure -save -set {PasswordManager}='none' 
    tools/configure -save -set {WebMasterEmail}="peter.l.jones@cern.ch" 
    tools/configure -save -set {EnableEmail}=1 
    tools/configure -save -set {Email}{MailMethod}='MailProgram' 
    tools/configure -save -set {MailProgram}='/usr/sbin/sendmail -t -oi -oeq' 
    tools/configure -save -set {AntiSpam}{EmailPadding}='NOSPAM' 
    tools/configure -save -set {ForceDefaultUrlHost}=1 
    tools/configure -save -set {INCLUDE}{AllowURLs}=1 
    tools/configure -save -set {UserInterfaceInternationalisation}=1 
    tools/configure -save -set {Languages}{fr}{Enabled}=1 
    tools/configure -save -set {AuthScripts}='attach,compareauth,configure,edit,manage,previewauth,diffauth,rdiffauth,rename,restauth,save,statistics,upload,viewauth,viewfileauth'   
    tools/configure -save -set  {Ldap}{AllowChangePassword}=0 
    tools/configure -save -set {Ldap}{Base}='DC=cern,DC=ch' 
    tools/configure -save -set  {Ldap}{BindDN}='' 
    tools/configure -save -set {Ldap}{BindPassword}='secret' 
    tools/configure -save -set {Ldap}{CaseSensitiveLogin}=0 
    tools/configure -save -set {Ldap}{CharSet}='' 
    tools/configure -save -set {Ldap}{Debug}=1 
    tools/configure -save -set {Ldap}{DefaultCacheExpire}='0' 
    tools/configure -save -set {Ldap}{Exclude}='WikiGuest, ProjectContributor, RegistrationAgent, UnknownUser, AdminGroup, NobodyGroup, AdminUser, admin, guest' 
    tools/configure -save -set {Ldap}{GroupAttribute}='cn' 
    tools/configure -save -set  {Ldap}{GroupBase}="[  'OU=e-groups,OU=Workgroups,DC=cern,DC=ch' ]" 
    tools/configure -save -set  {Ldap}{GroupFilter}='objectClass=group' 
    tools/configure -save -set {Ldap}{GroupScope}='sub' 
    tools/configure -save -set {Ldap}{Host}='xldap.cern.ch' 
    tools/configure -save -set {Ldap}{IPv6}=0 
    tools/configure -save -set {Ldap}{IgnorePrivateGroups}=1 
    tools/configure -save -set {Ldap}{IgnoreReferrals}=0 
    tools/configure -save -set {Ldap}{IgnoreViewRightsInSearch}=0 
    tools/configure -save -set {Ldap}{IndexEmails}=1 
    tools/configure -save -set {Ldap}{InnerGroupAttribute}='member' 
    tools/configure -save -set {Ldap}{KerberosKeyTab}='/etc/krb5.keytab' 
    tools/configure -save -set {Ldap}{LoginAttribute}='cn' 
    tools/configure -save -set {Ldap}{LoginFilter}='objectClass=organizationalPerson' 
    tools/configure -save -set {Ldap}{MailAttribute}='mail' 
    tools/configure -save -set {Ldap}{MapGroups}=1 
    tools/configure -save -set {Ldap}{MaxCacheAge}='0' 
    tools/configure -save -set {Ldap}{MemberAttribute}='member' 
    tools/configure -save -set {Ldap}{MemberIndirection}=1 
    tools/configure -save -set {Ldap}{MergeGroups}=1 
    tools/configure -save -set {Ldap}{NormalizeGroupNames}=0 
    tools/configure -save -set {Ldap}{NormalizeLoginNames}=0  
    tools/configure -save -set {Ldap}{NormalizeWikiNames}=1 
    tools/configure -save  -set {Ldap}{PageSize}='500' 
    tools/configure -save -set {Ldap}{PersonAttribures}="{
  'c' => 'Country',
  'company' => 'OrganisationName',
  'department' => 'Department',
  'division' => 'Division',
  'facsimileTelephoneNumber' => 'Telefax',
  'givenName' => 'FirstName',
  'l' => 'Location',
  'mail' => 'Email',
  'manager' => 'Manager',
  'mobile' => 'Mobile',
  'physicalDeliveryOfficeName' => 'Address',
  'postalAddress' => 'Address',
  'sAMAccountName' => 'LoginName',
  'sn' => 'LastName',
  'streetAddress' => 'Address',
  'telephoneNumber' => 'Telephone',
  'title' => 'Title',
  'uid' => 'LoginName'
}" 
    tools/configure -save -set {Ldap}{PersonDataForm}='UserForm' 
    tools/configure -save -set {Ldap}{Port}='389' 
    tools/configure -save -set {Ldap}{Precache}=1 
    tools/configure -save -set {Ldap}{PreferLocalSettings}=1 
    tools/configure -save -set {Ldap}{PrimaryGroupAttribute}='' 
    tools/configure -save -set {Ldap}{RewriteGroups}={} 
    tools/configure -save -set {Ldap}{RewriteLoginNames}={} 
    tools/configure -save -set {Ldap}{RewriteWikiNames}={ '^(.*)@.*$' => '\$1' }  
    tools/configure -save -set {Ldap}{SASLMechanism}='PLAIN CRAM-MD5 EXTERNAL ANONYMOUS' 
    tools/configure -save -set {Ldap}{SecondaryPasswordManager}='none' 
    tools/configure -save -set {Ldap}{TLSCAFile}='' 
    tools/configure -save -set {Ldap}{TLSCAPath}='' 
    tools/configure -save -set {Ldap}{TLSClientCert}='' 
    tools/configure -save -set {Ldap}{TLSClientKey}='' 
    tools/configure -save -set {Ldap}{TLSSSLVersion}='tlsv1' 
    tools/configure -save -set {Ldap}{TLSVerify}='required' 
    tools/configure -save -set {Ldap}{Timeout}='5' 
    tools/configure -save -set {Ldap}{UseCanonicalUserIDs}=0 
    tools/configure -save -set {Ldap}{UseSASL}=0 
    tools/configure -save -set {Ldap}{UseTLS}=0 
    tools/configure -save -set {Ldap}{UserBase}="[ 'OU=Users,OU=Organic Units,DC=cern,DC=ch',  'OU=Externals,DC=cern,DC=ch']" 
    tools/configure -save -set {Ldap}{UserMappingTopic}='' \
    tools/configure -save -set {Ldap}{UserScope}='sub' 
    tools/configure -save -set {Ldap}{Version}='3' 
    tools/configure -save -set {Ldap}{WikiGroupsBackoff}=1 
    tools/configure -save -set {Ldap}{WikiNameAliases}='' 
    tools/configure -save -set {Ldap}{WikiNameAttributes}='displayName, employeeType' 
#
    tools/configure -save -set {Extensions}{OpenID}{Default}{ClientID}='NAMESPACE'
    tools/configure -save -set {Extensions}{OpenID}{Default}{ClientSecret}='OIDC_CLIENT_SECRET'
    tools/configure -save -set {Extensions}{OpenID}{Default}{DiscoveryURL}='https://auth.cern.ch/auth/realms/cern/.well-known/openid-configuration'
    tools/configure -save -set {Extensions}{OpenID}{Default}{RedirectURL}='https://NAMESPACE.web.cern.ch/foswiki/bin/login'
#
    tools/configure -save -set {LoginManager}='Foswiki::LoginManager::OpenIDConnectLogin'
#
#
#LatexModePlugin needs the settngs done the old way
#
sed '/.*{Ldap}{AllowChangePassword}.*/i $Foswiki::cfg{LatexModePlugin}{latex}="/usr/bin/latex";\n$Foswiki::cfg{LatexModePlugin}{pdflatex}="/usr/bin/pdflatex";\n$Foswiki::cfg{LatexModePlugin}{dvips}="/usr/bin/dvips";\n$Foswiki::cfg{LatexModePlugin}{dvipng}="/usr/bin/dvipng";\n$Foswiki::cfg{LatexModePlugin}{convert}="/usr/X11R6/bin/convert";' lib/LocalSite.cfg > lib/LocalSite.cfg.tmp   
    mv lib/LocalSite.cfg.tmp lib/LocalSite.cfg
    #tools/bulk_copy.pl - to be run first time the data and pub directories are installed
    rm -fr /tmp/foswiki/working/configure/download/* 
    rm -fr /tmp/foswiki/working/configure/backup/*

 mkdir -p /foswiki/prod
 rsync -ah /tmp/foswiki/* /foswiki/prod
 #cp -r /tmp/foswiki/* /foswiki/prod/
#Set the permissions
#chmod -R 775 /foswiki/prod/data
#chmod -R 775 /foswiki/prod/pub
#chmod -R 775 /foswiki/prod/lib
#chmod -R 775 /foswiki/prod/working
cd /foswiki/prod
#sh tools/fix_file_permissions.sh
sed -i "s|/tmp/foswiki|/foswiki/prod|g" /foswiki/prod/lib/LocalSite.cfg
rm -rf /tmp/foswiki
#
chmod 777 /foswiki/prod/lib/Foswiki/Contrib/OpenIDLoginContrib/OpenIDConnect.pm
cp /foswiki/prod/lib/Foswiki/Contrib/OpenIDLoginContrib/OpenIDConnect.pm /foswiki/prod/lib/Foswiki/Contrib/OpenIDLoginContrib/OpenIDConnect.pm.orig 
cp /tmp/cern/OpenIDConnect.pm /foswiki/prod/lib/Foswiki/Contrib/OpenIDLoginContrib/OpenIDConnect.pm
cp /foswiki/prod/lib/Foswiki/Users.pm /foswiki/prod/lib/Foswiki/Users.pm.orig
cp /tmp/cern/Users.pm /foswiki/prod/lib/Foswiki/Users.pm
cp /foswiki/prod/lib/Foswiki/LoginManager.pm /foswiki/prod/lib/Foswiki/LoginManager.pm.orig
cp /tmp/cern/LoginManager.pm /foswiki/prod/lib/Foswiki/LoginManager.pm
cp /foswiki/prod/lib/Foswiki/Plugins/LdapNgPlugin/Core.pm /foswiki/prod/lib/Foswiki/Plugins/LdapNgPlugin/Core.pm.orig
cp /tmp/cern/Core.pm /foswiki/prod/lib/Foswiki/Plugins/LdapNgPlugin/Core.pm
cp /foswiki/prod/templates/openidloginbase.tmpl /foswiki/prod/templates/openidloginbase.tmpl.orig
cp /tmp/cern/openidloginbase.tmpl /foswiki/prod/templates/openidloginbase.tmpl
cp /tmp/cern/cern_logo.png /foswiki/prod/pub/System/OpenIDLoginContrib/cern_logo.png
# Make sure we're not confused by old, incompletely-shutdown httpd
# context after restarting the container.  httpd won't start correctly
# if it thinks it is already running.
rm -rf /run/apache2/*

echo "The Namespace is"
echo ${NAMESPACE}
echo "The secret is "
echo ${OIDC_CLIENT_SECRET}
#
#Put in the correct OIDC SECRET and NAMESPACE
sed "s/NAMESPACE/${NAMESPACE}/g" -i /foswiki/prod/lib/LocalSite.cfg
sed "s/NAMESPACE/${NAMESPACE}/g" -i /etc/apache2/conf.d/openidc.conf
sed "s/NAMESPACE/${NAMESPACE}/" -i /etc/apache2/conf.d/httpd10.conf
sed "s/OIDC_CLIENT_SECRET/${OIDC_CLIENT_SECRET}/" -i /foswiki/prod/lib/LocalSite.cfg
sed "s/OIDC_CLIENT_SECRET/${OIDC_CLIENT_SECRET}/" -i /etc/apache2/conf.d/openidc.conf
ls -al /etc/apache2/conf.d/openidc.conf
cat /etc/apache2/conf.d/openidc.conf
#apk del perl-cpanel-json-xs
hostname
date
echo "FINISHED"

exec /usr/sbin/httpd -D FOREGROUND

