FROM alpine:edge

LABEL "maintainer"="peter.l.jones@cern.ch"

ENV PERL_MM_USE_DEFAULT 1

ENV FOSWIKI_LATEST_URL https://github.com/foswiki/distro/releases/download/FoswikiRelease02x01x06/Foswiki-2.1.6.tgz

ENV FOSWIKI_LATEST_MD5 706fc6bf1fa6df6bfbe8a079c5007aa3

ENV FOSWIKI_LATEST Foswiki-2.1.6

#
#
ENV MOD_AUTH_OPENIDC_REPOSITORY https://github.com/zmartzone/mod_auth_openidc.git

ENV MOD_AUTH_OPENIDC_BRANCH master

ENV BUILD_DIR /tmp/mod_auth_openidc

ENV APACHE_LOG_DIR /var/log/apache2

ENV APACHE_DEFAULT_CONF /etc/apache2/httpd.conf

#
#
RUN rm -rf /var/cache/apk/* && \
    rm -rf /tmp/*

RUN sed -n 's/main/testing/p' /etc/apk/repositories >> /etc/apk/repositories && \
    apk update && \
    apk upgrade && \
    apk add --update && \
    apk add apache2  rcs rsync vim pandoc imagemagick texlive-dvi enscript wget mailcap musl tzdata bash nano glib curl libxslt libltdl texinfo texmf-dist-latexextra \
        grep unzip wget zip perl perl-algorithm-diff perl-algorithm-diff-xs \
        perl-apache-logformat-compiler perl-archive-zip perl-authen-sasl \
        perl-authcas perl-db perl-cache-cache perl-cgi perl-date-calc perl-cgi-session \
        perl-class-accessor perl-convert-pem perl-crypt-eksblowfish \
        perl-crypt-jwt perl-crypt-openssl-bignum perl-crypt-openssl-dsa \
        perl-crypt-openssl-random perl-crypt-openssl-rsa \
        perl-crypt-openssl-verifyx509 perl-crypt-openssl-x509 perl-crypt-passwdmd5 \
        perl-crypt-random perl-crypt-smime perl-crypt-x509 perl-dancer \
        perl-datetime perl-datetime-format-xsd perl-dbd-mysql perl-dbd-pg \
        perl-dbd-sqlite perl-db_file perl-db_file-lock perl-dbi \
        perl-devel-overloadinfo perl-digest-perl-md5 perl-digest-sha1 \
        perl-email-mime perl-encode perl-error perl-fcgi perl-fcgi-procmanager \
        perl-file-copy-recursive perl-file-remove perl-file-slurp perl-file-which \
        perl-filesys-notify-simple perl-file-which perl-gd perl-gssapi \
        perl-hash-merge-simple perl-hash-multivalue perl-html-tree  \
        perl-image-info perl-io-socket-inet6 perl-json  perl-json-xs \
        perl-ldap perl-libwww perl-locale-maketext-lexicon perl-locale-msgfmt \
        perl-lwp-protocol-https perl-mime-base64 perl-module-install \
        perl-module-pluggable perl-moo perl-moose perl-moosex \
        perl-moosex-types perl-moosex-types-common perl-locale-codes \
        perl-moosex-types-datetime perl-moosex-types-uri \
        perl-moox-types-mooselike perl-path-tiny perl-spreadsheet-parseexcel \
        perl-spreadsheet-xlsx perl-stream-buffered perl-sub-exporter-formethods \
        perl-sereal perl-test-leaktrace perl-text-unidecode perl-text-soundex perl-text-csv_xs \
        perl-time-parsedate perl-type-tiny perl-uri perl-www-mechanize \
        perl-xml-canonicalizexml perl-xml-easy perl-xml-generator perl-xml-parser \
        perl-xml-tidy perl-xml-writer perl-xml-xpath perl-yaml perl-yaml-tiny \
        perl-libapreq2  perl-file-mmagic perl-net-saml2  perl-ipc-run imagemagick-perlmagick graphviz \
        odt2txt antiword lynx poppler-utils perl-email-address-xs --update-cache && \
        # perl-libapreq2 -- Apache2::Request - Here for completeness but we use nginx \
        rm -fr /var/cache/apk/APKINDEX.*


RUN touch /root/.bashrc

#Install the lastest version of Foswiki
#This assumes that there is permanant storage nmes /foswiki
#
RUN wget ${FOSWIKI_LATEST_URL} && \
    echo "${FOSWIKI_LATEST_MD5}  ${FOSWIKI_LATEST}.tgz" > ${FOSWIKI_LATEST}.tgz.md5 && \
    md5sum -cs ${FOSWIKI_LATEST}.tgz.md5 && \
    mkdir -p /var/www && \
    mv ${FOSWIKI_LATEST}.tgz /tmp && \
    cd /tmp && \
    tar xvfz ${FOSWIKI_LATEST}.tgz && \
    rm -rf ${FOSWIKI_LATEST}.tgz && \
    mv ${FOSWIKI_LATEST} foswiki && \
    ln -s /foswiki/prod /var/www/localhost/htdocs/foswiki 

# Add extensions
RUN cd /tmp/foswiki && \ 
    tools/configure -save -noprompt && \
    tools/extension_installer ActionTrackerPlugin -r -enable install  && \ 
    tools/extension_installer AttachContentPlugin -r -enable install  && \ 
    tools/extension_installer AutoRedirectPlugin -r -enable install  && \ 
    tools/extension_installer AutoTemplatePlugin -r -enable install  && \ 
    tools/extension_installer BreadCrumbsPlugin -r -enable install  && \ 
    tools/extension_installer CalendarPlugin -r -enable install  && \ 
    tools/extension_installer ChartPlugin -r -enable install  && \ 
    tools/extension_installer NatSkin -r -enable install  && \ 
    tools/extension_installer JQPhotoSwipeContrib -r -enable install  && \ 
    tools/extension_installer CaptchaPlugin -r -enable install  && \ 
    tools/extension_installer ClassificationPlugin -r -enable install  && \ 
    tools/extension_installer CopyContrib -r -enable install  && \ 
    tools/extension_installer DBCacheContrib -r -enable install  && \ 
    tools/extension_installer DBCachePlugin -r -enable install  && \ 
    tools/extension_installer DiffPlugin -r -enable install  && \ 
    tools/extension_installer DigestPlugin -r -enable install  && \ 
    tools/extension_installer DirectedGraphPlugin -r -enable install  && \ 
    tools/extension_installer DirectedGraphWebMapPlugin -r -enable install  && \ 
    tools/extension_installer DocumentViewerPlugin -r -enable install  && \ 
    tools/extension_installer EditChapterPlugin -r -enable install  && \ 
    tools/extension_installer ExplicitNumberingPlugin -r -enable install  && \ 
    tools/extension_installer FarscrollContrib -r -enable install  && \ 
    tools/extension_installer FlexFormPlugin -r -enable install  && \ 
    tools/extension_installer FlexWebListPlugin -r -enable install  && \ 
    tools/extension_installer FilterPlugin -r -enable install  && \ 
    tools/extension_installer ForEachPlugin -r -enable install  && \ 
    tools/extension_installer GraphvizPlugin -r -enable install  && \ 
    tools/extension_installer GridLayoutPlugin -r -enable install  && \ 
    tools/extension_installer GnuPlotPlugin -r -enable install  && \ 
    tools/extension_installer ImageGalleryPlugin -r -enable install  && \ 
    tools/extension_installer ImagePlugin -r -enable install  && \ 
    tools/extension_installer IfDefinedPlugin -r -enable install  && \ 
    tools/extension_installer InfiniteScrollContrib -r -enable install  && \ 
    tools/extension_installer JHotDrawPlugin -r -enable install  && \ 
    tools/extension_installer JQDataTablesPlugin -r -enable install  && \ 
    tools/extension_installer JQMomentContrib -r -enable install  && \ 
    tools/extension_installer JQSelect2Contrib -r -enable install  && \ 
    tools/extension_installer JQSerialPagerContrib -r -enable install  && \ 
    tools/extension_installer JQTwistyContrib -r -enable install  && \ 
    tools/extension_installer JSTreeContrib -r -enable install  && \ 
    tools/extension_installer LdapContrib -r install  && \ 
    tools/extension_installer LatexModePlugin -r install  && \ 
    tools/extension_installer LdapNgPlugin -r install  && \ 
    tools/extension_installer LikePlugin -r -enable install  && \ 
    tools/extension_installer ListyPlugin -r -enable install  && \ 
    tools/extension_installer MathModePlugin -r -enable install  && \ 
    tools/extension_installer MarkdownPlugin -r -enable install  && \ 
    tools/extension_installer MediaElementPlugin -r -enable install  && \ 
    tools/extension_installer NatSkinPlugin -r -enable install  && \ 
    tools/extension_installer MetaCommentPlugin -r -enable install  && \ 
    tools/extension_installer MetaDataPlugin -r -enable install  && \ 
    tools/extension_installer MimeIconPlugin -r -enable install  && \ 
    tools/extension_installer MoreFormfieldsPlugin -r -enable install  && \ 
    tools/extension_installer MultiLingualPlugin -r -enable install  && \ 
    tools/extension_installer OpenIDLoginContrib -r -enable install  && \ 
    tools/extension_installer PerlPlugin -r -enable install  && \ 
    tools/extension_installer PageOptimizerPlugin -r -enable install  && \ 
    tools/extension_installer PubLinkFixupPlugin -r -enable install  && \ 
    tools/extension_installer NewUserPlugin -r -enable install  && \ 
    tools/extension_installer RedDotPlugin -r -enable install  && \ 
    tools/extension_installer RenderPlugin -r -enable install  && \ 
    tools/extension_installer SamlLoginContrib -r -enable install  && \ 
    tools/extension_installer SpreadsheetReaderPlugin -r -enable install  && \ 
    tools/extension_installer SecurityHeadersPlugin -r -enable install  && \ 
    tools/extension_installer StringifierContrib -r -enable install  && \ 
    tools/extension_installer SolrPlugin -r -enable install  && \ 
    tools/extension_installer SyntaxHighlightingPlugin -r -enable install  && \ 
    tools/extension_installer TagCloudPlugin -r -enable install  && \ 
    tools/extension_installer TimeTablePlugin -r -enable install  && \ 
    tools/extension_installer TopicInteractionPlugin -r -enable install  && \ 
    tools/extension_installer TopicTitlePlugin -r -enable install  && \ 
    tools/extension_installer TreePlugin -r -enable install  && \ 
    tools/extension_installer TreeBrowserPlugin -r -enable install  && \ 
    tools/extension_installer TWikiCompatibilityPlugin -r -enable install  && \ 
    tools/extension_installer WebLinkPlugin -r -enable install  && \ 
    tools/extension_installer WebFontsContrib -r -enable install  && \ 
    tools/extension_installer WorkflowPlugin -r -enable install  && \ 
    tools/extension_installer XSendFileContrib -r -enable install

# Upgrade existing packages in the base image
#RUN apk --no-cache upgrade

# Install apache from packages with out caching install files
#RUN apk add --no-cache apache2

RUN chmod -R 777 /var/log && \
    chmod -R 777 /var/www && \
    chmod -R 777 /run/apache2/ && \
    mkdir /tmp/cern 

RUN   sed -i "s|Listen 80|Listen 8080|g" /etc/apache2/httpd.conf && \
      #sed -i "s|#ServerName www.example.com:80|ServerName test-timlegge|g" /etc/apache2/httpd.conf && \
      sed -i "s|^\s*#LoadModule cgid_module modules/mod_cgid.so|LoadModule cgid_module modules/mod_cgid.so|g" /etc/apache2/httpd.conf && \
      sed -i "s|^\s*#LoadModule cgi_module modules/mod_cgi.so|LoadModule cgi_module modules/mod_cgi.so|g" /etc/apache2/httpd.conf && \
      sed -i '/LoadModule rewrite_module/s/^#//g' /etc/apache2/httpd.conf

COPY conf_files/httpd10.conf /etc/apache2/conf.d/httpd10.conf
COPY conf_files/10-foswiki.conf /etc/apache2/conf.d/10-foswiki.conf
COPY conf_files/openidc.conf /etc/apache2/conf.d/openidc.conf
COPY hello.pl /var/www/localhost/cgi-bin/hello.pl
COPY cern/OpenIDConnect.pm /tmp/cern/OpenIDConnect.pm
COPY cern/Core.pm  /tmp/cern/Core.pm
COPY cern/Users.pm /tmp/cern/Users.pm
COPY cern/LoginManager.pm /tmp/cern/LoginManager.pm
COPY cern/openidloginbase.tmpl /tmp/cern/openidloginbase.tmpl 
COPY cern/cern_logo.png /tmp/cern/cern_logo.png 


RUN chmod 777 /var/www/localhost/cgi-bin/hello.pl && \
    chmod -R 777  /etc/apache2/conf.d/ && \
    chmod -R 777 /tmp 

#
#Install openidc module for apache
#Hopefully this wont be needed in the future
# ADD source
RUN mkdir ${BUILD_DIR}

# add dependencies, build and install mod_auth_openidc, need atomic operation for image size
RUN apk update && apk add --no-cache \
  apache2-proxy \
  wget \
  jansson \
  hiredis \
  cjose \
  cjose-dev \
  git \
  autoconf \
  build-base \
  automake \
  apache2-dev \
  curl-dev \
  pcre-dev \
  xmlsec \
  libtool \
  && \
  cd ${BUILD_DIR} && \
  git clone -b ${MOD_AUTH_OPENIDC_BRANCH} ${MOD_AUTH_OPENIDC_REPOSITORY} && \
  cd mod_auth_openidc && \
  ./autogen.sh && \
  ./configure CFLAGS="-g -O0" LDFLAGS="-lrt" && \
  make test && \
  make install && \
  cd ../.. && \
  rm -fr ${BUILD_DIR} && \
  apk del git cjose-dev apache2-dev autoconf automake build-base wget curl-dev pcre-dev libtool


# configure apache 
RUN  apk add --no-cache sed && \
  echo "LoadModule auth_openidc_module /usr/lib/apache2/mod_auth_openidc.so" >>  ${APACHE_DEFAULT_CONF}
#
#
# Open port for httpd access
EXPOSE 8080

# Run httpd in foreground so that the container does not quit
# soon after start
# To run this container in the back ground use the -d option
#
#     $ sudo docker run -d broadtech/alpine-apache2
#
#CMD ["-D","FOREGROUND"]

# Srart httpd when container runs
#ENTRYPOINT ["/usr/sbin/httpd","-X"]
#
#RUN mv /usr/bin/cpanel_json_xs /usr/bin/cpanel_json_xs.old
#RUN mv /usr/bin/json_xs /usr/bin/json_xs.old
#RUN chmod 777 /usr/share/perl5/vendor_perl/JSON/MaybeXS.pm

RUN chmod g+w /etc/passwd

COPY  run-apache.sh /run-apache.sh

RUN chmod -v +x /run-apache.sh

CMD ["/run-apache.sh"]

